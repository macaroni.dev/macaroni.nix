#! /usr/bin/env bash

set -ex

repo_root=$(git rev-parse --show-toplevel)

cd "$repo_root"

readarray -t examples <<< $(fd -j1 Makefile $PWD/examples --exec echo {//})

echo "Found examples: ${examples[@]}"

for e in ${examples[@]}; do
    echo "Building $e"
    pushd $e
    make build
    make windows
    popd
done

echo "All examples built successfully!"
