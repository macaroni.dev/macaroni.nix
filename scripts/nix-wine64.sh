#! /usr/bin/env nix-shell
#! nix-shell -i bash -p coreutils -p wineWowPackages.stable

# TODO: Nixify this and include it in the macaroni.nix package set

# Originally from https://gist.github.com/ramirez7/9fc5bc79ef7962b431c6601598b9130b

set -e

if [[ -z "$1" ]]; then
  echo "USAGE: ./nix-wine-run.sh PATH_TO_EXE"
  exit 1
fi

# We don't need Gecko or Mono:
export WINEDLLOVERRIDES='mscoree=d;mshtml=d'

# Create a temporary WINEPREFIX for isolation:
export WINEPREFIX=$(mktemp -d)
trap "rm -rf $WINEPREFIX" EXIT

WINEDEBUG='-all' wine64 "$1"
