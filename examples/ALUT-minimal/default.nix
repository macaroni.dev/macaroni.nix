let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "ALUT-minimal";
    src = ./../..;
    subDir = "examples/ALUT-minimal";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
