Getting `h-raylib` working for both Linux and Windows requires two things:

1. Use a version of the library that has support for Nix: At least `4.5.0.3`.
  - `h-raylib >= 4.5.0.3` in your `.cabal` file will do it.
2. Use the `mingw-cross` flag when x-compiling for Windows. [See this example's `cabal.project` for how to do that.](cabal.project)

## Implementation Notes

`h-raylib` declares `libc` and `libdl` as `extra-libraries`. This causes `haskell.nix` to try to find them in `nixpkgs`. We must map them to `null` with this overlay to satisfy `haskell.nix`:

```nix
self: super:
{
  c = null;
  dl = null;
}
```

The `mingw-cross` flag causes `h-raylib` to declare a `gcc` `extra-library` dependency when compiling to Windows. This is necessary for `haskell.nix` to bundle DLLs (e.g. `mcfgthread-12.dll`) with the Windows `.exe`.
