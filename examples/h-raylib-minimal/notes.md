- `X11/extensions/Xge.h` was not found. I tried adding `Xext` to `h-raylib`'s `extra-libraries` in a fork.
  - That worked! I can now pop a "Hello, World" window.

---------------

I get a bunch of these warnings in `cabal repl`. They don't seem to break anything though:

```
<no location info>: warning: [-Wmissed-extra-shared-lib]
    libGL.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libX11.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libXinerama.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libXcursor.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libXrandr.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libXi.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).

<no location info>: warning: [-Wmissed-extra-shared-lib]
    libXext.so: cannot open shared object file: No such file or directory
    It's OK if you don't want to use symbols from it directly.
    (the package DLL is loaded by the system linker
     which manages dependencies by itself).
```

----------------

`mcfgthread-12.dll` was missing (not copied into `result`). I added `gcc` to `h-raylib`'s Windows `extra-libraries` and that gets it to work.

I don't know if this is a great way to accomplish that though.
