let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.cabalProject {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "h-raylib-minimal";
    src = ./../..;
    subDir = "examples/h-raylib-minimal";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
