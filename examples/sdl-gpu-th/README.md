# 2023-07-01

https://github.com/input-output-hk/haskell.nix/issues/1990

## The error

iserv seems to be trying to create a `bin` directory in the `sdl-gpu` Nix store path:

```
Preprocessing executable 'sdl-gpu-th' for sdl-gpu-th-0.1.0.0..
Building executable 'sdl-gpu-th' for sdl-gpu-th-0.1.0.0..
[1 of 1] Compiling Main             ( src/Main.hs, dist/build/sdl-gpu-th/sdl-gpu-th-tmp/Main.o )
---> Starting remote-iserv.exe on port 9300
---| remote-iserv.exe should have started on 9300
Could not find Wine Gecko. HTML rendering will be disabled.
wine: configuration in L"/build" has been updated.
Listening on port 9300
remote-iserv.exe: \nix\store\kmn1hv32hvq89g1h0s82zkgzdkpysk8g-SDL_gpu-x86_64-w64-mingw32-2019-01-24\bin: CreateDirectory "\\\\?\\Z:\\nix\\store\\kmn1hv32hvq89g1h0s82zkgzdkpysk8g-SDL_gpu-x86_64-w64-mingw32-2019-01-24\\bin": permission denied (Access denied.)
iserv-proxy: {handle: <socket: 6>}: GHCi.Message.remoteCall: end of file
ghc: ghc-iserv terminated (1)
```

## The fix

Create empty `$out/bin` directories in the `SDL_gpu` and `SDL_FontCache` derivations.
