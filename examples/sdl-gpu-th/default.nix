let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.cabalProject {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "sdl-gpu-th";
    src = ./../..;
    subDir = "examples/sdl-gpu-th";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
