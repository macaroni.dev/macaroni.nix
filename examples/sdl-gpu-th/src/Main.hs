{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
-- For optics labels:
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- This is Lesson01 from https://github.com/palf/haskell-sdl2-examples
module Main (main) where

import qualified SDL
import qualified SDL.GPU.Simple as GPU
import qualified SDL.GPU.FC.Simple as FC

import Foreign.Ptr
import Linear.V2

import Data.Function (fix)
import System.Exit (die)
import Control.Monad

import Optics

data Test = Test { test :: Int }

$(Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels ''Test)

main :: IO ()
main = putStrLn "Hello sdl-gpu TH!"
