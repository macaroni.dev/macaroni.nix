let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "gloss-minimal";
    src = ./../..;
    subDir = "examples/gloss-minimal";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
