# `gloss-minimal` example

The `Makefile` provides some targets to build and run the example (both on Linux and Windows).

## GLUT workaround
Note that we must add this config to our cabal file because `haskell.nix` doesn't propagate `gloss`'s dependency on GLUT correctly. These lines will ensure we have that native dependency in scope:

```
  -- We have to do this because our gloss/GLUT dependencies don't automatically
  -- propagate this C dependency for some reason
  if os(windows)
    pkgconfig-depends:
      freeglut
  else
    pkgconfig-depends:
      glut
```
