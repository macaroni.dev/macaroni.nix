{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ForeignFunctionInterface #-}
module Lib where

import Foreign.C.String

foreign import ccall "SDL.h SDL_GetRevision" getRevisionFFI :: IO CString
