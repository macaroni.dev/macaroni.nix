let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "sdl2-pkgconfig-minimal";
    src = ./../..;
    subDir = "examples/sdl2-pkgconfig-minimal";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
