{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ForeignFunctionInterface #-}

module Main (main) where

import Lib (getRevisionFFI)
import Foreign.C.String

main :: IO ()
main = getRevisionFFI >>= peekCString >>= \s -> putStrLn $ "Hello SDL rev " ++ s
