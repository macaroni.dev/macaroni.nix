let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.project {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "sdl2-th";
    src = ./../..;
    subDir = "examples/sdl2-th";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
