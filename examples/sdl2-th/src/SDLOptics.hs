{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FunctionalDependencies #-}

module SDLOptics where

import SDL
import Optics.TH
import Data.Word
import Data.Int

import SDLOptics.Rules (rules)

makePrisms ''SDL.EventPayload

makeLensesWith rules ''SDL.Event

makeLensesWith rules ''SDL.KeyboardEventData

makeLensesWith rules ''SDL.Keysym

makeLensesWith rules ''SDL.MouseMotionEventData

makeLensesWith rules ''SDL.MouseButtonEventData

makeLensesWith rules ''SDL.MouseWheelEventData
