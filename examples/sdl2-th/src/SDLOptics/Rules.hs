module SDLOptics.Rules where

import Optics
import Optics.TH
import Language.Haskell.TH
import Data.List (stripPrefix)
import Data.Char (toLower, toUpper)

rules :: LensRules
rules =
  noPrefixFieldLabels
  & lensField .~ underscorePrefix

underscorePrefix :: FieldNamer
underscorePrefix = \_ _ -> pure . TopName . mkName . ('_' :) . upperFirst . nameBase

upperFirst :: String -> String
upperFirst [] = []
upperFirst (s : rest) = toUpper s : rest
