{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main (main) where

import qualified SDL
import qualified SDLOptics as SDL
import Optics
import Control.Monad          (void, unless, guard)
import Control.Monad.IO.Class (MonadIO)
import Data.Text              (Text)
import Data.Maybe (mapMaybe)

main :: IO ()
main = withSDL $ withWindow "Mouse Color Changer" (640, 480) $
  \w -> do
    screen <- SDL.getWindowSurface w
    let loop r g b = do
          events <- SDL.pollEvents
          let incVal x = if x == maxBound - 20 then minBound else x + 20
              r' = if wasMousePressed SDL.ButtonLeft events then incVal r else r
              g' = if wasMousePressed SDL.ButtonMiddle events then incVal g else g
              b' = if wasMousePressed SDL.ButtonRight events then incVal b else b
          SDL.surfaceFillRect screen Nothing (SDL.V4 r' g' b' maxBound)
          SDL.updateWindowSurface w
          SDL.delay 16
          unless (SDL.QuitEvent `elem` fmap (view SDL._EventPayload) events) (loop r' g' b)

    loop maxBound maxBound maxBound
    SDL.freeSurface screen

withSDL :: (MonadIO m) => m a -> m ()
withSDL op = do
  SDL.initialize []
  void op
  SDL.quit

withWindow :: (MonadIO m) => Text -> (Int, Int) -> (SDL.Window -> m a) -> m ()
withWindow title (x, y) op = do
  w <- SDL.createWindow title p
  SDL.showWindow w
  void $ op w
  SDL.destroyWindow w

    where
      p = SDL.defaultWindow { SDL.windowInitialSize = z }
      z = SDL.V2 (fromIntegral x) (fromIntegral y)

wasMousePressed :: SDL.MouseButton -> [SDL.Event] -> Bool
wasMousePressed button es = not $ null $ flip mapMaybe es $ \e -> do
  SDL.MouseButtonEventData{..} <- e ^? SDL._EventPayload % SDL._MouseButtonEvent
  guard (button == mouseButtonEventButton)
  guard (mouseButtonEventMotion == SDL.Pressed)
  pure ()
