# TH

Template Haskell used to not work when x-compiled to Windows. The iserv process would hang. It was possible to work around it with a very hacky bash script that used TH splices.

[It recently got fixed.](https://github.com/input-output-hk/haskell.nix/pull/1974) This example hangs around as a sanity check that TH continues to work.
