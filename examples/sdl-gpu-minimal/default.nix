let
  pkgs = import ./../.. {};
in pkgs.haskell-nix.cabalProject {
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "sdl-gpu-minimal";
    src = ./../..;
    subDir = "examples/sdl-gpu-minimal";
  };

  compiler-nix-name = "ghc928";
  evalPackages = pkgs;
}
