This example uses the "simple" bindings. An example with the old "C-level" bindings can be found [here](https://gitlab.com/macaroni.dev/macaroni.nix/-/blob/808a01bee5b834df9cb44be1ac2dea25686ffb72/examples/sdl-gpu-minimal/src/Main.hs).

![What this example does](./screen.png)
