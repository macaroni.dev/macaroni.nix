{-# LANGUAGE OverloadedStrings #-}

-- This is Lesson01 from https://github.com/palf/haskell-sdl2-examples
module Main (main) where

import qualified SDL
import qualified SDL.GPU.Simple as GPU
import qualified SDL.GPU.FC.Simple as FC

import Foreign.Ptr
import Linear.V2

import Data.Function (fix)
import System.Exit (die)
import Control.Monad

main :: IO ()
main = do
  GPU.setDebugLevel GPU.debugLevelMax

  screen <- GPU.init 800 600 GPU.defaultInitFlags

  when (screen == nullPtr) $ die "Failed to GPU.init"

  let redColor = GPU.Color 255 0 0 255
  let whiteColor = GPU.Color 255 255 255 255
  freeSans <- FC.loadFont "FreeSans.ttf" 20 whiteColor 0

  fix $ \loop -> do
    events <- SDL.pollEvents
    GPU.clear screen
    _ <- FC.draw freeSans screen 10 10 "Hello, FreeSans"
    GPU.polygonFilled screen [V2 50 50, V2 50 100, V2 100 66, V2 110 78] redColor
    GPU.flip screen
    SDL.delay 16
    let shouldQuit = SDL.QuitEvent `elem` fmap SDL.eventPayload events
    unless shouldQuit loop

  FC.freeFont freeSans
  GPU.quit
