# Getting Started

`macaroni.nix` is a thin wrapper around `haskell.nix` (mostly overlays to fix gamedev libraries for cross-compilation), so you can [follow the `haskell.nix` guide](https://input-output-hk.github.io/haskell.nix/tutorials/getting-started.html) with some slight differences:

`macaroni.nix` makes some tweaks pretty deep in the Windows toolchain up to and including a patch to MinGW-w64. This means you won't get many cache hits when cross-compiling from `haskell.nix`'s cache, so you will have long build times. Soon, we will provide a public `macaroni.nix` cache to help with this.

[For Niv](https://github.com/nmattia/niv), add `macaroni.nix` instead of `haskell.nix`. We pin a `haskell.nix` under the hood for you:

```
niv init
niv add git https://gitlab.com/macaroni.dev/macaroni.nix.git -n macaroniNix
```

You can then update to the latest version of `macaroni.nix` with:

```
niv update macaroniNix
```

`macaroni.nix`'s top-level is a `nixpkgs` itself. So you can tweak the example `haskell.nix` scaffolding like so:

```nix
# default.nix
let
  sources = import ./nix/sources.nix {};
  pkgs = import sources.macaroniNix {};
in pkgs.haskell-nix.cabalProject { # macaroni.nix is currently only tested with cabal
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "haskell-nix-project";
    src = ./.;
  };
  # Specify the GHC version to use.
  compiler-nix-name = "ghc928"; # macaroni.nix is currently only tested on GHC 9.2.8

  # This is necessary for x-compilation
  # See https://github.com/input-output-hk/haskell.nix/issues/1666
  evalPackages = pkgs;
}
```

You can work with the project with the same commands `haskell.nix` uses.

[You should also be able to get a development shell the same way with `shellFor`.](https://input-output-hk.github.io/haskell.nix/tutorials/development/) In this shell, you can develop your game with `cabal` like a normal Haskell project.

You can cross-compile your project to Windows with a single command:

```
nix-build -A projectCross.mingwW64.hsPkgs.your-package.components.exes.your-game
```

The output directory will include your `.exe` along with a bunch of `.dll`s needed to run your game. You can bundle these together when distributing your game to your players.

You can confirm your game works on Windows locally with `wine64`:

```
$ nix-shell -p wineWowPackages.stable

[nix-shell]$ WINE_PREFIX=~/.wine64 wine64 ./result/bin/game.exe
```
