# Design

`macaroni.nix` is a thin wrapper around `haskell.nix`. The user can use it exactly like `haskell.nix`. The main thing `macaroni.nix` does is use overlays to modify packages so they are gamedev-friendly (e.g. cross-compilation to Windows).

`macaroni.nix` pins `haskell.nix` and `hackage.nix` itself. This is mostly because cross-compilation is often broken as upstream dependencies change. If you wish, you can override them by passing `haskellNixSrc` / `hackageNixSrc` when importing `macaroni.nix`.

It would be good to upstream the changes we make in our overlays to `nixpkgs`, but for now they're staying in-tree. The spirit of `macaroni.nix` is that it's the frontline for Haskell gamedev x-compilation in a world of changing GHCs, `haskell.nix`, and `nixpkgs`. Because those projects don't focus on Haskell gamedev, they sometimes introduce issues for it. Whereas `macaroni.nix`'s primary goal is the gamedev.

While `macaroni.nix` is focused on Windows x-compilation today, the long-term goal is to support all gamedev target platforms: Windows, Mac, Linux, iOS, Android, ARM (RPi), JS/Wasm, even consoles. In an ideal world, a Haskell gamedev will effortlessly release their game to all platforms simply by building different Nix targets. Focus on building your game - not your build!
