# Support Status

Currently, only Windows cross-compilation is supported. In fact, the easiest way to make your game run portably on Linux is probably to cross-build it to Windows and run it with `wine64`.

## Native Libraries

The following native libraries have been properly packaged for Windows:

- [SDL2](https://www.libsdl.org/)
- [SDL2_ttf](https://github.com/libsdl-org/SDL_ttf)
- [SDL_gpu](https://github.com/grimfang4/sdl-gpu)
- [SDL_FontCache](https://github.com/grimfang4/SDL_FontCache)
- [OpenAL Soft](https://openal-soft.org/)
- [freealut](https://github.com/vancegroup/freealut)
- [freeglut](http://freeglut.sourceforge.net/)

## Haskell

`macaroni.nix` has only been tested with GHC 9.2.8, although it should work more or less with others. **NOTE: On 9.2.8, you will want to set `exactDeps = false` in your `shell.nix` if you are using `source-repository-package`s due to [this upstream issue](
https://github.com/input-output-hk/haskell.nix/issues/1608).**

As a rule, pure Haskell libraries have no trouble cross-compiling.

The following Haskell gamedev libraries have been cross-compiled successfully with `macaroni.nix`:

- [`sdl2`](https://hackage.haskell.org/package/sdl2)
- [`sdl2-ttf`)(https://hackage.haskell.org/package/sdl2-ttf)
- [`OpenAL`](https://hackage.haskell.org/package/OpenAL) and [`ALUT`](https://hackage.haskell.org/package/ALUT)
- [`gloss`](https://hackage.haskell.org/package/gloss)
  - And transitively [`GLUT`](https://hackage.haskell.org/package/GLUT) and [`OpenGL`](https://hackage.haskell.org/package/OpenGL)
  - NOTE: You need to add some config to your cabal file to make it work 100%. [See the `gloss` example for more details.](../examples/gloss-minimal)
- [`hgeometry`](https://hackage.haskell.org/package/hgeometry)
- [`h-raylib` as of version `4.5.0.3`](https://hackage.haskell.org/package/h-raylib)
  - See [the `h-raylib` example](../examples/h-raylib-minimal/) for details on how to cross-compile.

The following unpublished Haskell libraries also work:

- [`sdl-gpu`](https://gitlab.com/macaroni.dev/sdl-gpu-hs) (also contains `SDL_FontCache` bindings)
- [`cute-c2`](https://gitlab.com/macaroni.dev/cute-c2-hs) and [`cute-sound`](https://gitlab.com/macaroni.dev/cute-sound-hs)

They are not published to Hackage, so you must specify them in your `cabal.project` file. Here are their known-working `source-repository-package` specifications for reference:
```
source-repository-package
    type: git
    location: https://gitlab.com/macaroni.dev/sdl-gpu-hs.git
    tag: 4a95b553fadfd3d78b278eff6a47398a0a15179c
    --sha256: sha256-xlv+k4uV4kukrtVGJtbgG4Bbbm/uEiJgOwPM4ppsFCk=

source-repository-package
    type: git
    location: https://gitlab.com/macaroni.dev/cute-sound-hs.git
    tag: 710d39826a4ed69ba0cc02644632e26489846101
    --sha256: 1gh08i3q7fabf9jrvi72kgfja5byx9yrsi4yqayh5k2smqkry3i6

source-repository-package
    type: git
    location: https://gitlab.com/macaroni.dev/cute-c2-hs.git
    tag: c22e31ba97b7a9a6aa7e2dd2f4aa35860a75cfbb

```

## Known Issues

You can browse [the `support` label](https://gitlab.com/macaroni.dev/macaroni.nix/-/issues/?label_name%5B%5D=support) for a complete list of known issues. The most likely issues you'll run into are:

- `hsc2hs` is extremely slow when cross-compiling. [[Upstream Issue]](https://github.com/input-output-hk/haskell.nix/issues/36)
- Not all native libraries get their DLLs automatically bundled. Notably, `zlib` and `SDL2_ttf`.
