{ sources ? import ./nix/sources.nix {}
, haskellNixSrc ? sources.haskellNix
, hackageNixSrc ? sources.hackageNix # TODO: Bring this back
}:

let macaroniNix = rec {
      haskellNix = import haskellNixSrc {};

      mkPkgs =
        {
          config ? haskellNix.nixpkgsArgs.config,
          overlays ? haskellNix.nixpkgsArgs.overlays,
          extraOverlays ? 
          [ (import ./nix/overlays/macaroniLib)
            (import ./nix/overlays/gamedev)
            (import ./nix/overlays/windows)
          ]
        }:
        (import haskellNix.sources.nixpkgs-unstable # TODO: Allow for user-defined nixpkgs sources
          {
            inherit config;
            overlays = overlays ++ extraOverlays;
          });
    };
in macaroniNix.mkPkgs {}
