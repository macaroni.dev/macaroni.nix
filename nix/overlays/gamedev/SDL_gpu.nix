self: super:

let srcHEAD = self.fetchFromGitHub {
      owner = "ramirez7";
      repo = "sdl-gpu";
      rev = "455214775214da77526bfc6d65c7cd986d5384f6";
      sha256 = "sha256-QA41I8OphPvfpsTh62eSZoAZ/BSyCzSl+RP8dQ51xss=";
    };
in {
  haskell-nix = super.macaroniLib.mapExtraPkgconfig super.haskell-nix "SDL_gpu" ["SDL_gpu"];
  SDL_gpu = super.SDL_gpu.overrideAttrs (old: {
    src = srcHEAD;

    pname = "SDL_gpu";

    # We don't need libGLU on Windows - mingw provides the OpenGL dlls we need
    buildInputs = if !self.stdenv.hostPlatform.isWindows then old.buildInputs else [ self.SDL2 ];

    # We don't seem to need pkg-config - we manually make the pc file below
    #
    # We could potentially bring in buildPackages.pkg-config
    nativeBuildInputs = old.nativeBuildInputs;

    cmakeFlags = [
      "-DBUILD_DEMOS=OFF"
      "-DBUILD_TOOLS=OFF"
      "-DBUILD_VIDEO_TEST=OFF"
      "-DBUILD_TESTS=OFF"
      "-DBUILD_DOCS=OFF"
      "-DINSTALL_LIBRARY=ON" # Windows sets this to OFF
    ];

    preInstall = ''
      mkdir -p $out

      # demos
      #cp -r demos $out
      #cp -r $src/demos/data $out/demos

      #tests
      #cp -r tests $out

      # docs
      #make doc
      #mkdir -p $out/docs
      #cp -r html $out/docs

      # We need this empty bin/ dir to make iserv happy:
      # https://github.com/input-output-hk/haskell.nix/issues/1990
      mkdir -p $out/bin

      # pkg-config
      mkdir -p $out/lib/pkgconfig
      cat >> $out/lib/pkgconfig/SDL_gpu.pc << EOF
      prefix=@CMAKE_INSTALL_PREFIX@
      exec_prefix=''${prefix}
      libdir=''${exec_prefix}/lib
      includedir=''${prefix}/include

      Name: SDL_gpu
      Description: SDL_gpu
      Version: ${old.version}
      Requires.private: sdl2
      Libs: -L$out/lib -lSDL2_gpu
      Libs.private: @private_libs@
      Cflags: -I$out/include/SDL2

      EOF
    '';
  });
}
