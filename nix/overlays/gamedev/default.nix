# Additional gamedev packages that are either missing from nixpkgs
# or need significant modifications for general Haskell integration
let
  overlays = {
    SDL_gpu = import ./SDL_gpu.nix;
    SDL_FontCache = import ./SDL_FontCache.nix;
    h-raylib-extra-libraries = import ./h-raylib-extra-libraries.nix;
  };
in
self: super:
super.macaroniLib.deepComposeExtensions overlays self super
