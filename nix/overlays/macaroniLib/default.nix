self: super:
{
  macaroniLib = rec {
    deepAttrValues = any:
      if builtins.isAttrs any
      then builtins.concatMap (value:
        if builtins.isAttrs value
        then deepAttrValues value
        else [value]
      ) (builtins.attrValues any)
      else [any]
    ;

    deepComposeExtensions = overlays: super.lib.composeManyExtensions (deepAttrValues overlays);

    # See https://stackoverflow.com/a/54505212
    recursiveMerge = x: y: recursiveMergeAll [x y];
    recursiveMergeAll = attrList:
      with super.lib;
      let f = attrPath:
            zipAttrsWith (n: values:
              if tail values == []
              then head values
              else if all isList values
              then unique (concatLists values)
              else if all isAttrs values
              then f (attrPath ++ [n]) values
              else last values
            );
      in f [] attrList;

    mapExtraPkgconfig = super-haskell-nix: name: mappings: recursiveMerge super-haskell-nix {
      extraPkgconfigMappings = { "${name}" = mappings; };
    };
  };
}
