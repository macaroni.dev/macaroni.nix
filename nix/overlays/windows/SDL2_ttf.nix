self: super:
{
  SDL2_ttf = super.SDL2_ttf.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    # On Windows for some reason, it can't find freetype-config
    FT2_CONFIG = "${self.freetype.dev}/bin/freetype-config";
    SDL2_CONFIG = "${self.SDL2.dev}/bin/sdl2-config";
    buildInputs = self.lib.lists.remove self.libGL old.buildInputs ++ [ self.freetype.dev ];
  });
}

