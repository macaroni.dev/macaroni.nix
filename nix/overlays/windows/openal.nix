{
  linuxAudio = self: super:
    {
      openal = super.openal.override ({
        alsaSupport = self.stdenv.hostPlatform.isLinux;
        dbusSupport = self.stdenv.hostPlatform.isLinux;
        pipewireSupport = self.stdenv.hostPlatform.isLinux;
        pulseSupport = self.stdenv.hostPlatform.isLinux;
      });
    };

  xcompile = self: super:
    let preserveInstallPrefixPatch =
          self.writeText "preserve-install-prefix.patch" ''
diff --git a/XCompile.txt b/XCompile.txt
index 32706bc1..cdfc2e4e 100644
--- a/XCompile.txt
+++ b/XCompile.txt
@@ -15,7 +15,7 @@ SET(CMAKE_RC_COMPILER "''${HOST}-windres")
 SET(CMAKE_FIND_ROOT_PATH "/usr/''${HOST}")
 
 # here is where stuff gets installed to
-SET(CMAKE_INSTALL_PREFIX "''${CMAKE_FIND_ROOT_PATH}" CACHE STRING "Install path prefix, prepended onto install directories." FORCE)
+SET(CMAKE_INSTALL_PREFIX "''${CMAKE_FIND_ROOT_PATH}" CACHE STRING "Install path prefix, prepended onto install directories.")
 
 # adjust the default behaviour of the FIND_XXX() commands:
 # search headers and libraries in the target environment, search 
          '';
    in {
      openal = super.openal.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {

        cmakeFlags = old.cmakeFlags ++ [
          "-DCMAKE_TOOLCHAIN_FILE=../XCompile.txt"
          "-DHOST=x86_64-w64-mingw32"
        ];

        # openal-soft's XCompile.txt FORCEs the CMAKE_INSTALL_PREFIX to be in /usr
        # We need it to be in the nix store. All the patch does is remove FORCE so it
        # doesn't overwrite what we set
        patches = old.patches ++ [ preserveInstallPrefixPatch ];

        # freealut's cmake looks for "openal" and "openal32" but not "OpenAL32" for some reason
        #
        # Also, nixpkgs openal does some Linux-specific stuff for pipewire that we want to clobber.
        postInstall = ''
        ln -s $out/lib/libOpenAL32.dll.a $out/lib/libopenal32.dll.a
        '';
      });
    };

  map32 = self: super:
    {
      OpenAL32 = self.openal;
    };
}
