{
  x11 = self: super:
    {
      SDL2 = super.SDL2.override ({
        # X11 is a Linux and Darwin thing
        x11Support = !self.stdenv.hostPlatform.isWindows && !self.stdenv.isCygwin && !self.stdenv.hostPlatform.isAndroid;
      });
    };
  noPatch = self: super:
    {
      SDL2 = super.SDL2.overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else {
        # nixpkgs has a bunch of Linux-specific OpenGL & Wayland things in postPatch
        postPatch = "";
      });
    };
  postInstall = self: super:
    {
      SDL2 = super.SDL2.overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else {
        # I arrived at this due to trial-and error. It looks suspiciously similar to what
        # nixpkgs does, but not quite.
        postInstall = ''
        rm $out/lib/*.la
        moveToOutput bin/sdl2-config "$dev"
        '';
      });
    };
}
