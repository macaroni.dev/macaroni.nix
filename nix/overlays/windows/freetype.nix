self: super:
{
  libpng = super.libpng.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    # freetype on windows complains about the presence of the .la file
    # removing it seems to work
    postInstall = ''
      echo "HELLO LIBPNG FIXUP FOR WINDOWS"
      rm $out/lib/libpng16.la
    '';
  });
  freetype = super.freetype.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    # cribbed from https://github.com/NixOS/nixpkgs/issues/160323
    # postInstall = glib.flattenInclude + '' # flattenInclude may be fine
    #   substituteInPlace $dev/bin/freetype-config \
    #     --replace ${buildPackages.pkg-config} ${pkgsHostHost.pkg-config}
    #   ^ This pkgsHostHost seems to be the mingwW64 pkg-config we're building
    #   wrapProgram "$dev/bin/freetype-config" \
    #     --set PKG_CONFIG_PATH "$PKG_CONFIG_PATH:$dev/lib/pkgconfig"
    #   ^ wrapProgram brings in bash..we can use makeBinaryWrapper as a drop-in replacement
    # '';
    nativeBuildInputs = [];
    postInstall = "";
  });
}
