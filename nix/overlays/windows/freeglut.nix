self: super:
{
  haskell-nix = super.macaroniLib.mapExtraPkgconfig super.haskell-nix "freeglut" ["freeglut"];
  freeglut = super.freeglut.overrideAttrs(old: if !self.stdenv.hostPlatform.isWindows then {} else {
    buildInputs = [ ];
    cmakeFlags = old.cmakeFlags ++ [
      # See freeglut's README.mingw_cross
      "-DCMAKE_TOOLCHAIN_FILE=mingw_cross_toolchain.cmake"
      "-DGNU_HOST=x86_64-w64-mingw32"
    ];
  });
}
