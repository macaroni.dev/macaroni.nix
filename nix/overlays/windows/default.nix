# Overlays of nixpkgs packages for better Windows support
let
  overlays = {
    SDL2 = import ./SDL2.nix;
    SDL2_ttf = import ./SDL2_ttf.nix;
    freetype = import ./freetype.nix;
    freeglut = import ./freeglut.nix;
    openal = import ./openal.nix;
    freealut = import ./freealut.nix;
  };
in
self: super:
super.macaroniLib.deepComposeExtensions overlays self super

