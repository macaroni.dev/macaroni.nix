{
  cmake = self: super: {
    freealut = super.freealut.overrideAttrs (old: if !self.stdenv.hostPlatform.isWindows then {} else {
      # freealut's README says that cmake is the best choice nowadays (especially for cross-building)
      nativeBuildInputs = (old.nativeBuildInputs or []) ++ [ self.buildPackages.cmake self.buildPackages.pkg-config ];
      # freealut needs to be explicitly told where OpenAL is
      cmakeFlags = (old.cmakeFlags or []) ++ [
        "-DOPENAL_LIB_DIR=${self.openal}/lib"
        "-DOPENAL_INCLUDE_DIR=${self.openal}/include"
      ];
    });
  };
}
