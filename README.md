# `macaroni.nix`

`macaroni.nix` is the Nix code used by us (macaroni.dev) to develop our Haskell games on Linux (NixOS) and cross-build and release them to Windows.

If you are a Haskell gamedev interested in developing on Linux and releasing to Windows without having to maintain a parallel Windows build system, this project is for you.

## Documentation

- [Getting Started](docs/getting-started.md) - How to use `macaroni.nix`
- [Support Status](docs/support-status.md) - What works and what doesn't
- [Design Overview](docs/design.md)
- [Built with `macaroni.nix`](docs/built-with.md)

## Contributing and Getting Help

`macaroni.nix` is still in early development, and so it still needs elbow grease to be ready for mass consumption. That said, please give it a try!

Feel free to cut an issue to this repository if you run into trouble. Even if your issue is upstream, I would love to help figure it out. This is bleeding edge tech after all!

If you want to contribute, just trying `macaroni.nix` out with your Haskell game is a good start. Trying to get it working with other gamedev libraries (some notable ones are Vulkan, sokol, and GPipe) and figuring out what overlays and `haskell.nix` changes we need is even better - we want as broad of support as we can get.

[`niv` has pretty good support for using local sources for testing stuff out.](https://github.com/nmattia/niv#can-i-use-local-packages) And you can easily [override the pinned `haskell.nix` and `hackage.nix` when you import `macaroni.nix`.](https://gitlab.com/macaroni.dev/macaroni.nix/-/blob/master/default.nix#L2-3) If you run into trouble, feel free to cut an issue for the errors your game is hitting and we can work together to improve `macaroni.nix` for everyone.
